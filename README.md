# GitTools

(en français plus bas)
Tools for Git: 

- UserConfig - initial user configuration: 1 file, common to all users (option to delete local branches and tags removed on the central repo, option to use `rebase` with `pull`),
 1 file to be modified for each user (name and e-mail)
- DiffTool_MergeTool_Setup: init scripts to use an external graphical *diff* software with *difftool & mergetool*
- GitK_here: call *gitK* with right click from *Windows Files Explorer*

---
Outils pour Git : 

- UserConfig - configuration initiale d'un utilisateur: 1 fichier commun à tous les utilisateurs (option pour supprimer les branches locales et les tags sur le dépôt central, option pour utiliser `rebase` avec `pull`),
 1 fichier à modifier pour chaque utilisateur (nom et e-mail)
- DiffTool_MergeTool_Setup : scripts d'initialisation pour utiliser un logiciel externe graphique de *diff* avec *difftool & mergetool*
- GitK_here : appeler *gitK* par un clic droit depuis l'*Explorateur de Fichiers Windows*
